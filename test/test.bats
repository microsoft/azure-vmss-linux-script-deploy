#!/usr/bin/env bats

setup() {
    DOCKER_IMAGE=${DOCKER_IMAGE:="test/azure-vmss-linux-script-deploy"}

    # Test based on
    # https://docs.microsoft.com/en-us/azure/virtual-machine-scale-sets/tutorial-install-apps-cli

    # generated
    RANDOM_NUMBER=$RANDOM

    # required globals - stored in Pipelines repository variables
    AZURE_APP_ID="${AZURE_APP_ID}"
    AZURE_PASSWORD="${AZURE_PASSWORD}"
    AZURE_TENANT_ID="${AZURE_TENANT_ID}"

    # required globals - generated
    AZURE_RESOURCE_GROUP="test${RANDOM_NUMBER}"
    AZURE_VMSS_NAME="myvmss${RANDOM_NUMBER}"
    
    # locals - generated
    LB_NAME="${AZURE_VMSS_NAME}LB"
    LB_RULE_NAME="${AZURE_VMSS_NAME}LBRule"
    BACKEND_POOL_NAME="${AZURE_VMSS_NAME}LBBEPool"
    PUBLIC_IP_NAME="${AZURE_VMSS_NAME}LBPublicIP"
    AZURE_VM_ADMIN_USERNAME="user$RANDOM_NUMBER"

    # locals - fixed
    LOCATION="westus2"
    IMAGE="UbuntuLTS"
    UPGRADE_POLICY_MODE="automatic"
    BACKEND_PORT="80"
    FRONTEND_IP_NAME="loadBalancerFrontEnd"
    FRONTEND_PORT="80"
    PROTOCOL="TCP"

    echo "Building image..."
    docker build -t ${DOCKER_IMAGE}:0.1.0 .

    echo "Creating required Azure resources"
    az login --service-principal --username ${AZURE_APP_ID} --password ${AZURE_PASSWORD} --tenant ${AZURE_TENANT_ID}
    az group create --name ${AZURE_RESOURCE_GROUP} --location ${LOCATION}
    az vmss create --resource-group ${AZURE_RESOURCE_GROUP} --name ${AZURE_VMSS_NAME} --image ${IMAGE} --upgrade-policy-mode ${UPGRADE_POLICY_MODE} --admin-username ${AZURE_VM_ADMIN_USERNAME} --generate-ssh-keys
    az network lb rule create --resource-group ${AZURE_RESOURCE_GROUP} --name ${LB_RULE_NAME} --lb-name ${LB_NAME} --backend-pool-name ${BACKEND_POOL_NAME} --backend-port ${BACKEND_PORT} --frontend-ip-name ${FRONTEND_IP_NAME} --frontend-port ${FRONTEND_PORT} --protocol ${PROTOCOL}
}

teardown() {
    echo "Clean up Resource Group"
    az group delete -n ${AZURE_RESOURCE_GROUP} --yes
}

@test "Script can be run on Azure VM Scale Set using Custom Script Extension Version 2" {
    # required globals - fixed
    AZURE_EXTENSION_COMMAND="./automate_nginx.sh"

    # optional globals - fixed
    AZURE_EXTENSION_FILES="https://raw.githubusercontent.com/Azure-Samples/compute-automation-configurations/master/automate_nginx.sh"

    echo "Run test"
    run docker run \
        -e AZURE_APP_ID="${AZURE_APP_ID}" \
        -e AZURE_PASSWORD="${AZURE_PASSWORD}" \
        -e AZURE_TENANT_ID="${AZURE_TENANT_ID}" \
        -e AZURE_RESOURCE_GROUP="${AZURE_RESOURCE_GROUP}" \
        -e AZURE_VMSS_NAME="${AZURE_VMSS_NAME}" \
        -e AZURE_EXTENSION_COMMAND="${AZURE_EXTENSION_COMMAND}" \
        -e AZURE_EXTENSION_FILES="${AZURE_EXTENSION_FILES}" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:0.1.0

    echo ${output}
    [[ "$status" -eq 0 ]]
    [[ "${output}" == *"Deployment successful."* ]]

    PUBLIC_IP_ADDRESS=$(az network public-ip show --resource-group ${AZURE_RESOURCE_GROUP} --name ${PUBLIC_IP_NAME} --query ipAddress | sed -e 's/^"//' -e 's/"$//')
    run curl --silent "http://${PUBLIC_IP_ADDRESS}"

    echo ${output}
    [[ "${status}" -eq 0 ]]
    [[ "${output}" == *"Hello World from host"* ]]
}

