FROM microsoft/azure-cli:2.0.57

COPY pipe /
RUN apk add --update coreutils
RUN chmod a+x /*.sh

ENTRYPOINT ["/pipe.sh"]

